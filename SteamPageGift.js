// ==UserScript==
// @name         SteamPageGift
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.steamgifts.com/giveaway/*
// @grant        none
// ==/UserScript==


(function() {
    'use strict';
    var time = 3*1000+7*1000*Math.random();
    var reg = RegExp('is-hidden');
    var link = window.location.href.replace('https://www.steamgifts.com','');
    /*
    if(!reg.test($('form [data-do="entry_insert"]').attr("class"))){
        console.log('Start Insert ...');
        //console.log(reg.test($('form [data-do="entry_insert"]').attr("class")));
        $('form [data-do="entry_insert"]').click();
        console.log('finish Insert ...');
    }else if(!reg.test($('form [data-do="entry_delete"]').attr("class"))){
        console.log('Start Delete ...');
        //console.log(reg.test($('form [data-do="entry_delete"]').attr("class")));
        $('form [data-do="entry_delete"]').click();
        console.log('finish Delete ...');
    }*/
    var tempGameList	= JSON.parse(localStorage.gameList);
    var flag = true;
    for (var j in tempGameList){
        if(link==tempGameList[j].link){
            flag = tempGameList[j].entered;
            console.log('target:',tempGameList[j]);
            break;}}
    if(flag)$('form [data-do="entry_insert"]').click();
    else $('form [data-do="entry_delete"]').click();

    console.log('Will close in ',time,' minisec');
    setTimeout(function(){window.close();},time);
    // Your code here...
})();

// ==UserScript==
// @name         SteamGift Take it all
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  try to take over the world!
// @author       You
// @match        https://www.steamgifts.com/giveaways/search?type=wishlist
// @grant        none
// ==/UserScript==


//show ur point
function getPointer(){
    $.get('https://www.steamgifts.com/about/faq',function(result){
        pointNum = parseInt($(result).find('.nav__points').text());
        //console.log('当前点数: ',pointNum);
        localStorage.pointNum	= pointNum;
        $('.nav__points').text(localStorage.pointNum);
        getStat();
    });
}
//show ur stat
function getStat(){
    if(pointNum>=pointNum_upperlimited)stat = 2;
    else if(pointNum>=pointNum_lowerlimited)stat = 1;
    else stat = 0;
    //console.log('stat',stat);
    //console.log('pointNum',pointNum);
}
//must change
/*
function getRealtime(time){
    var reg = new RegExp('[0-9]+');
    var s = RegExp('second');
    var m = RegExp('minute');
    var h = RegExp('hour');
    var d = RegExp('day');
    var w = RegExp('week');
    //console.log('origin',time);
    //console.log('number',time.match(reg)[0]);
    //console.log(m.test(time));
    //console.log(h.test(time));
    //console.log(w.test(time));
    if(s.test(time)){
        //console.log('origin:',time,',timeSec',parseInt(time.match(reg)[0])*1000);
        return parseInt(time.match(reg)[0])*1000;
    }else if(m.test(time)){
        //console.log('origin:',time,',timeMin',parseInt(time.match(reg)[0])*60*1000);
        return parseInt(time.match(reg)[0])*60*1000;
    }else if(h.test(time)){
        //console.log('origin:',time,',timeHour',parseInt(time.match(reg)[0])*60*60*1000);
        return parseInt(time.match(reg)[0])*60*60*1000;
    }else if(d.test(time)){
        //console.log('origin:',time,',timeDay',parseInt(time.match(reg)[0])*60*60*24*1000);
        return parseInt(time.match(reg)[0])*60*60*24*1000;
    }else if(w.test(time)){
        //console.log('origin:',time,',timeWeek',parseInt(time.match(reg)[0])*60*60*24*7*1000);
        return parseInt(time.match(reg)[0])*60*60*24*7*1000;
    }else{
        //console.log('SomethingWrong');
        return null;
    }
}*/
//update list
function updateList(url){
    $.get(url, function(HTMLData) {
        $(HTMLData).find(".giveaway__row-outer-wrap").each(function() {
            var gameUrl = $(this).find(".giveaway__heading a").attr("href");
            var reg = new RegExp('[0-9]+');
            var regcp = new RegExp('Copies');
            console.log("Find Game: ",gameUrl);
            var flag_History = true;
            if (typeof(localStorage.gameList)=='undefined' || localStorage.gameList=='[null]')localStorage.gameList = "[]";
            var tempGameList	= JSON.parse(localStorage.gameList);
            var index = tempGameList.length;
            for (var j in tempGameList){
                if(gameUrl+'#OneClickEnter'==tempGameList[j].link){
                    flag_History=false;
                    index = j;
                    break;
                }else{index = tempGameList.length;}
            }
            console.log(($(this).find('.giveaway__heading')[0].getElementsByTagName("span")[0].textContent.replace(',','')));
            console.log('regcp',regcp.test(($(this).find('.giveaway__heading')[0].getElementsByTagName("span")[0].textContent.replace(',',''))));
            var cp = (regcp.test(($(this).find('.giveaway__heading')[0].getElementsByTagName("span")[0].textContent.replace(',',''))))?parseInt(($(this).find('.giveaway__heading')[0].getElementsByTagName("span")[0].textContent.replace(',','')).match(reg)[0]):1;
            console.log('cp',cp);
            tempGameList[index]	=
                {'name':($(this).find('.giveaway__heading')[0].getElementsByTagName("a")[0].textContent),
                 'link':gameUrl+'#OneClickEnter','pic':'gamePic',
                 'hash':gameUrl.split("/")[2],
                 'participants':parseInt(($(this).find('.giveaway__links')[0].getElementsByTagName("span")[0].textContent.replace(',','')).match(reg)[0]),
                 'copies':(regcp.test(($(this).find('.giveaway__heading')[0].getElementsByTagName("span")[0].textContent.replace(',',''))))?parseInt(($(this).find('.giveaway__heading')[0].getElementsByTagName("span")[0].textContent.replace(',','')).match(reg)[0]):1,
                 'point':parseInt(('target',$(this).find(".giveaway__heading__thin")[0].textContent).match(reg)[0]),
                 'deadline':(parseInt($(this).find(".giveaway__columns")[0].getElementsByTagName("span")[0].getAttribute("data-timestamp")))*1000,
                 'deadline_origin':(new Date((parseInt($(this).find(".giveaway__columns")[0].getElementsByTagName("span")[0].getAttribute("data-timestamp"))+serverTimeBias)*1000)).toISOString(),
                 'entered':$(this).find('.giveaway__row-inner-wrap.is-faded').length==1
                };
            //console.log('new',tempGameList[index]);
            localStorage.gameList	= JSON.stringify(tempGameList);
            //console.log("tempHistory",tempGameList);
            //}
        });
    });
}

function opentabs(){
    //console.log(localStorage.SGH_gameList);
    //kill it all
    if (typeof(localStorage.gameList)!='undefined'){
        var tempGameList	= JSON.parse(localStorage.gameList);
        if (tempGameList.length>0){
            tempGameList.sort(sortFunc);
            console.log('tempGameList_sorted :',JSON.stringify(tempGameList));
            var counter = 0;
            for (i = 0 ; i < tempGameList.length;i++){
                console.log('this is ',i,'logic',((tempGameList[i].deadline - getRealTime()) > 0),
                            'time_bias',(tempGameList[i].deadline - getRealTime()),'Detail :',tempGameList[i]);
                //console.log('deadline',new Date(tempGameList[i].deadline));
                //console.log('realtime',new Date(getRealTime()));
                if((tempGameList[i].deadline - getRealTime()) > 0){subopentabs(counter,tempGameList[i]);counter++;}
            }
            localStorage.gameList	= JSON.stringify(tempGameList);
        }
    }
}

function subopentabs(i,tmp){
    var sleepawhile = opentab_asleep*i+3*Math.random();
    setTimeout(function(){
        console.log('------------------------------');
        console.log('id:',i,'Detail:',tmp);
        console.log('deadline',new Date(tmp.deadline),'tmp.entered',tmp.entered);
        console.log('stat :',stat,',pointNum',pointNum,',sleepawhile',sleepawhile/1000,'s ',',start id :',i);
        console.log('time_left:',(tmp.deadline-getRealTime())/(60*60*1000),'h ',
                    ',timeLimitMostTake',timeLimitMostTake/(60*60*1000),'h ',
                    'biasTime',(tmp.deadline-(getRealTime()-timeLimitMostTake)/(60*60*1000)),'h ',
                    ',checked :',(stat==2 || (stat==1 && tmp.deadline <= timeLimitUpperMostTake)|| (stat==0 && tmp.deadline <= timeLimitMostTake)));
        console.log('Copies',tmp.copies,'participants',tmp.participants,'LotteryRate',(tmp.copies/tmp.participants));
        if(tmp.entered==true && (tmp.copies/tmp.participants) < lowestLotteryRate){
            var tempGameList	= JSON.parse(localStorage.gameList);
            for (var j in tempGameList){
                if(tmp.link==tempGameList[j].link){
                    tempGameList[j].entered=false;
                    console.log('target:',tempGameList[j]);
                    break;
                }
            }
            localStorage.gameList = JSON.stringify(tempGameList);
            console.log('Decision: remove process :',tmp.name,tmp.hash);
            if(testMode==false)window.open(tmp.link);
            console.log('Decision: remove finished :',tmp.name,tmp.hash);
        }else if((stat==2 || (stat==1 && (tmp.deadline - getRealTime()) <= timeLimitUpperMostTake)|| (stat==0 && (tmp.deadline - getRealTime()) <= timeLimitMostTake)) && (pointNum - tmp.point >= 0) && (tmp.copies/tmp.participants) > lowestLotteryRate){
            pointNum -= tmp.point;
            var tempGameList	= JSON.parse(localStorage.gameList);
            var counter = 0;
            console.log('Decision: insert process :',tmp.name,tmp.hash);
            for (var j in tempGameList){
                if(tmp.link==tempGameList[j].link){
                    tempGameList[j].entered=true;
                    console.log('target:',tempGameList[j]);
                    break;
                }
            }
            localStorage.gameList = JSON.stringify(tempGameList);
            getStat();
            if(testMode==false)window.open(tmp.link);
            console.log('Decision: insert finished :',tmp.name,tmp.hash);
        }else{console.log('Decision: pass');}
        console.log('------------------------------');
    },sleepawhile);
}

function sleep(milliseconds) {
    var start = getRealTime();
    //console.log(start);
    for (var i = 0; i < 1e7; i++) {
        //console.log((getRealTime()));
        if ((getRealTime() - start) > milliseconds){
            break;
        }
    }
}

function sleeps (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

function timecheck(){
    var d = new Date();
    console.log('timelog:',(d-starttime)/60000,' min  ,','count :',counter-180+1);
    counter++;
}

function getRealTime(){
    //console.log((new Date(new Date().getTime())));
    return (new Date().getTime());}

function sortFunc(a,b){
    if(a.deadline < b.deadline)return -1;
    else if(a.deadline > b.deadline)return 1;
    else return 0;}


var delay4getPoint = 5*1000;
var sleeptime = 0.2*60*60*1000 +0.01*60*60*1000*Math.random();
//var duringtime = 2*sleeptime;
var slice = 1000;
var starttime = new Date();
var counter = 0;
var testMode = false;
var opentab_asleep = testMode?0:10*1000;
var pointNum = 0;
var pointNum_upperlimited = 400;
var pointNum_lowerlimited = 200;
var timeLimitUpperMostTake = 23*60*60*1000;
var timeLimitMostTake = -1 + 1*60*60*1000;
var statusTimeBias = 24*60*60;
//show statistic range
var serverTimeBias = 8*60*60;
var lowestLotteryRate = 1/600;
var stat = 0;
//3 stat
var url = '';
//var processDelay	= 3000;
var loopCounter = 1;
//console.log(sleeptime/(60*60*1000));
(function() {
    'use strict';
    // Your code here...

    //myLoop();
    getPointer();
    if (typeof(localStorage.gameList)!='undefined'){
        var timestamp = getRealTime();
        var tempGameList	= JSON.parse(localStorage.gameList);
        var sum = 0;
        var count = 0;
        //console.log('start');
        console.log('tempGameList0',tempGameList);
        tempGameList.sort(sortFunc);
        console.log('tempGameList1',tempGameList);
        var deleteList = [];
        for(var i in tempGameList){
            if(timestamp >= tempGameList[i].deadline && tempGameList[i].deadline >= timestamp-statusTimeBias){
                console.log(1,(timestamp >= tempGameList[i].deadline && tempGameList[i].deadline >= timestamp-statusTimeBias));
                sum+=1;
                if(tempGameList[i].entered == true){
                    count+=1;
                }
            }else if(timestamp >= tempGameList[i].deadline && (timestamp-statusTimeBias) >= tempGameList[i].deadline ){
                console.log(2,(tempGameList[i].deadline < timestamp-statusTimeBias));
                console.log('delete',tempGameList[i],timestamp - tempGameList[i].deadline,timestamp,tempGameList[i].deadline);
                deleteList[deleteList.length] = tempGameList[i];
            }else console.log(3,tempGameList);
        }
        for(var i in deleteList){
            //console.log('delete',deleteList[i]);
            tempGameList.splice(tempGameList.indexOf(deleteList[i]),1);}
        console.log('deletelist',deleteList);
        console.log('tempGameList3',tempGameList);
        localStorage.gameList = JSON.stringify(tempGameList);
        if(sum>0){console.log('Status_SuccessRate_24_hr :',count/sum,'count',count,'sum',sum);}
    }
    //var tempGameList	= JSON.parse(localStorage.gameList);
    //console.log('tempGameList3',tempGameList);
    for(counter=1;counter<180;counter++){sleeps(60000*counter).then(() => {timecheck();});}
    sleeps(delay4getPoint*2).then(() => {
        updateList('https://www.steamgifts.com/giveaways/search?type=wishlist');
        updateList('https://www.steamgifts.com/giveaways/search?type=group');
    });
    sleeps(delay4getPoint*3).then(() => {
        console.log('Startup Current Point : ',pointNum,'Startup Current stat : ',stat,'Start time :',starttime);
        opentabs();
    });
    sleeps(sleeptime).then(() => {
        window.location.reload();// 这里写sleep之后需要去做的事情
    });
})();
